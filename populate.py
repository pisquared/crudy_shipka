#!/usr/bin/env python
# coding=utf-8

def populate():
    from shipka.store import database
    from shipka.store.database import User

    admin_user = User.get_one_by(id=1)
    # #####################################
    # Create your populations here
    # #####################################
    database.add(admin_user)