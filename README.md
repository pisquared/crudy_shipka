# CRUDy - Shipka Integration

This project works in minimal isolation with [shipka](https://gitlab.com/pisquared/shipka) and [CRUDy](https://gitlab.com/pisquared/crudy) to create a proof of concept for integration.
