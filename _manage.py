#!/usr/bin/env python
"""
Used for easy integration with python-based IDEs like PyCharm.
"""

import eventlet
import sys

if sys.argv[1] != 'shell':
    eventlet.sleep()
    eventlet.monkey_patch()

if __name__ == '__main__':
    from shipka.manager import manager

    manager.run()
