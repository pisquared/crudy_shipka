from flask import render_template
from shipka.webapp.views.crudy_integration import build_ctx

from webapp.client import client


@client.route('/')
def get_home():
    ctx = build_ctx()
    return render_template('index.html',
                           **ctx)


@client.route('/service-worker.js')
def get_service_worker_js():
    return client.send_static_file('js/service-worker.js')


@client.route('/offline')
def get_offline():
    ctx = build_ctx()
    return render_template('offline.html',
                           **ctx)
