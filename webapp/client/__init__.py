from flask import Blueprint, current_app

client = Blueprint('client', __name__,
                   static_folder='static',
                   static_url_path='/static/client',
                   template_folder='templates')

from . import views
