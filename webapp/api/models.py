"""
Define your models here, like so:

```
from shipka.store.search import whooshee

from shipka.store.database import db, ModelController, Datable, Ownable


@whooshee.register_model('body')
class BlogPost(db.Model, ModelController, Datable, Ownable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)
```
"""

from shipka.store.database import db, ModelController, Datable, Ownable


class BlogPost(db.Model, ModelController, Datable, Ownable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)
