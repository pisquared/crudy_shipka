"""
Define view_models like so:

```
MODELS_REGISTRY.update({
    'blog_post': {
        'vm': ModelView,
        'model': BlogPost,
        # The ones below are optional
        form_include: [],
        form_exclude: [],
        form_only: [],
        creatable: True,
        editable: True,
        deletable: True,
        user_needed: True,
        creatable_login_required: ['admin', ]
        'creatable_roles_required': ['admin', ]
    },
})
```
"""

from shipka.webapp.registries import MODELS_REGISTRY, update_model_registry
from shipka.webapp.util import ModelView

from webapp.api.models import BlogPost
from webapp.api import api

MODELS_REGISTRY.update({
    'blog_post': {
        'vm': ModelView,
        'model': BlogPost,
    },
})

update_model_registry(api)
